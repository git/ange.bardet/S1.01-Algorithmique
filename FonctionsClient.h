#include <stdio.h>

/**
*\file FonctionsClient.h
*\brief fichier.h qui contient les déclarations des fonctions concernant le client.
*\author BARDET Ange & Mathis CHIRAT
*\date 20 oct 2023
*/


/**
*\brief Demande a l'utilisateur son code client ainsi que la charge et le volume de son véhicule. La fonction charge également la cagnotte du client.
*\param[in,out] chargeV pointeur vers flottant, la charge totale du véhicule du client (en kg).
*\param[in,out] codeC pointeur vers entier, le code qui permet d'identifier le client.
*\return flottant, le volume (en L) du coffre du véhicule du client.
*/
float initClient(float *chargeV, int *codeC, float *cagnotte);

/**
*\brief Demande un article a l'utilisateur puis l'ajoute au panier.
*\param[in,out] TrefC tableau d'entier, articles dans le panier client.
*\param[out] TquanC tableau d'entier, quantités dans le panier client.
*\param[in,out] nArticle entier, taille logique des tableaux concernant le panier.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return entier, nombre d'article + 1.
*/
int ajoutPanier(int TrefC[],int TquanC[],int nArticle,int Tref[], int n);

/**
*\brief Affiche le panier actuel du client.
*\param[in] TrefC tableau d'entier, articles dans le panier client.
*\param[in] TquanC tableau d'entier, quantités dans le panier client.
*\param[in] nArticle entier, taille logique des tableaux concernant le panier.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] Tpoid tableau d'entier, poids des produits.
*\param[in] Tvolume tableau d'entier, volumes des produits.
*\param[in] Tprix tableau d'entier, prix des produits.
*\param[in] chargeV flottant, la charge totale du véhicule du client (en kg).
*\param[in] volV flottant, le volume (en L) du coffre du véhicule du client.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return Rien
*/
void affichPanier(int TrefC[],int TquanC[],int nArticle,int Tref[],float Tpoid[],float Tvolume[],float Tprix[],float chargeV, float volV,int n, float cagnotteC);

/**
*\brief Demande un article au client puis le modifie sa quantité.
*\param[in] TrefC tableau d'entier, articles dans le panier client.
*\param[out] TquanC tableau d'entier, quantités dans le panier client.
*\param[in] nArticle entier, taille logique des tableaux concernant le panier.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return Rien
*/
void ModifArticlePanier(int TrefC[],int TquanC[],int nArticle,int Tref[],int n);

/**
*\brief Demande un article au client puis le supprime du panier.
*\param[in,out] TrefC tableau d'entier, articles dans le panier client.
*\param[out] TquanC tableau d'entier, quantités dans le panier client.
*\param[in] nArticle entier, taille logique des tableaux concernant le panier.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return entier, taille logique des tableaux concernant le panier -1.
*/
int SupprArticlePanier(int TrefC[],int TquanC[],int nArticle,int Tref[],int n);

/**
*\brief Réinitialise le panier du client.
*\param[out] TrefC tableau d'entier, articles dans le panier client.
*\param[out] TquanC tableau d'entier, quantités dans le panier client.
*\param[in] nArticle entier, taille logique des tableaux concernant le panier.
*\return entier, taille logique des tableaux concernant le panier.
*/
int ResetPanier(int TrefC[],int TquanC[], int nArticle);

/**
*\brief Recherche la position de nArticle dans le tableau TrefC.
*\param[in] TrefC tableau d'entier, articles dans le panier client.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\param[in] refR entier, réfèrence recherché dans le tableau référence.
*\param[in] nArticle entier, taille logique des tableaux concernant le panier.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\return entier, position de la réfèrence recherché.
*/
int posRefC(int TrefC[], int n, int refR,int nArticle,int Tref[]);