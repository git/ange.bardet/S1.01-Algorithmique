#include "FonctionsClient.h"
#include "FonctionsResponsable.h"
#include <stdlib.h>
#include <stdio.h>

/**
*\file FonctionsClient.c
*\brief fichier.c qui contient toutes les fonctions concernant le client.
*\author BARDET Ange & Mathis CHIRAT
*\date 20 oct 2023
*/

float initClient(float *chargeV,int *codeC, float *cagnotte)
{
	int tmax=100,n,pos;
	int tabNC [100]={0}, tabsuspension [100]={0};
	float tabCAG [100]={0};
	n = tableaucharge(tabNC,tabCAG,tabsuspension,tmax)+1;
	float volV;
	printf("Quel est votre code client ? : ");
	scanf("%d",codeC);
	pos = posRef(tabNC,*codeC,n);
	if (pos >= 0)
	{
		printf("Votre cagnotte actuelle est de %.2f\n",tabCAG[pos]);
		*cagnotte = tabCAG[pos];
	}
	if (pos >= 0 && tabsuspension[pos] == 1)
	{
		printf("La carte est suspendu, vous n'avez pas accès a votre cagnotte.\n");
	}
	if (pos < 0)
	{
		printf("Vous n'êtes pas encore référencée dans les clients.\n");
	}
    
	printf("Quel est le volume (L) du coffre de votre véhicule : ");
	scanf("%f",&volV);
    while (volV < 0)
    {
        printf("Erreur ; Le volume doit être positif ; retapez : ");
        scanf("%f",&volV);
    }
	printf("Quel est la charge totale de votre véhicule (Kg) : ");
	scanf("%f",chargeV);
    while (*chargeV < 0)
    {
        printf("Erreur ; La charge doit être positive ; retapez : ");
        scanf("%f",chargeV);
    }

	return volV;
}

int ajoutPanier(int TrefC[],int TquanC[],int nArticle,int Tref[], int n)
{
	int refP,code,quantite,pos;
	printf("Saisir la référence du produit à ajouter au panier: ");
	scanf("%d",&refP);

	pos = posRefC(TrefC,n,refP,nArticle,Tref);
	if (pos != -1)
	{
		printf("L'article existe déja dans le panier.");
		return nArticle;
	}

	code = posRef(Tref,refP,n);
	while (code == -1)
	{
		printf("Erreur ; Cette référence n'existe pas dans la base de donnée; retapez : ");
		scanf("%d",&refP);
		code = posRef(Tref,refP,n);
	}

	printf("Saisir la quantité souhaité pour le produit: ");
	scanf("%d",&quantite);
	while (quantite < 1)
	{
		printf("Erreur ; La quantité peut seulement être positive; retapez : ");
		scanf("%d",&quantite);
	}
	printf("\nRéférence : %d\n",refP);
	printf("Quantité : %d\n",quantite);
	TrefC[nArticle] = refP;
	TquanC[nArticle] = quantite;
	return nArticle +=1;
} 

void affichPanier(int TrefC[],int TquanC[],int nArticle,int Tref[], float Tpoid[],float Tvolume[],float Tprix[],float chargeV, float volV,int n, float cagnotteC)
{
	printf("\nRéf\tQté\tPoids\tVol\tPrixU\tPoidsTot\tVolTot\tPrixTot\tCagnotte\n");
	int i, pos;
	float prixtot,volR,chargeR,cagnotte;
	float cfinal=0;
	float prixfinal=0,volfinal=0,chargefinal=0;
	for (i=0;i <nArticle;i++)
	{
		pos = posRef(Tref,TrefC[i],n);
		prixtot=Tprix[pos]*TquanC[i];
		cagnotte=prixtot*0.1;
		prixfinal+=prixtot;
		cfinal+=cagnotte;
		volfinal+=Tvolume[pos]*TquanC[i];
		chargefinal+=Tpoid[pos]*TquanC[i];
		printf("%d\t%d\t%.1f\t%.1f\t%.1f\t%.1f\t\t%.1f\t%.1f\t%.2f\n",TrefC[i],TquanC[i],Tpoid[pos],Tvolume[pos],Tprix[pos],Tpoid[pos]*TquanC[i],Tvolume[pos]*TquanC[i],prixtot,cagnotte);
	}
	printf("\n\t\t\t\t\t\tPrix total à payer:\t%.2f euros\n",prixfinal);
	printf("\t\t\t\t\t\tCagnotte totale:\t%.2f euros\n",cfinal+cagnotteC);
	
	volR = volV - volfinal;
	printf("Volume utilisé:  %.1f litres\n",volfinal);
	printf("Volume restant: ");
	if (volR < 0)
	{
		printf("Attention dépassement de la charge autorisée de ");
		volR *= -1;
	}
	printf("%.1f litres\n",volR);

	chargeR = chargeV - chargefinal;
	printf("Charge Actuelle: %.1f kg\n",chargefinal);
	printf("Charge restante: ");
	if (chargeR < 0)
	{
		printf("Attention dépassement de la charge autorisée de ");
		chargeR *= -1;
	}
	printf("%.1f kg\n",chargeR);
}

void ModifArticlePanier(int TrefC[],int TquanC[],int nArticle,int Tref[],int n)
{
	if (nArticle <= 0)
	{
		printf("Il n'y a aucun article dans le panier.");
		return;
	}
	int refR,pos,nQuan;
	printf("Saisir la référence de l'article a modifier: ");
	scanf("%d",&refR);
	pos = posRefC(TrefC,n,refR,nArticle,Tref);
	if (pos == -1)
	{
		printf("L'article n'existe pas dans le panier.");
	}
	else
	{
		printf("Saisir la nouvelle quantité du produit: ");	
		scanf("%d",&nQuan);
		TquanC[pos] = nQuan;
	}
}

int SupprArticlePanier(int TrefC[],int TquanC[],int nArticle,int Tref[],int n)
{
	if (nArticle <= 0)
	{
		printf("Il n'y a aucun article dans le panier.");
		return nArticle;
	}
	int refR,pos;
	printf("Saisir la référence de l'article a supprimer: ");
	scanf("%d",&refR);
	pos = posRefC(TrefC,n,refR,nArticle,Tref);
	if (pos == -1)
	{
		printf("L'article n'existe pas dans le panier.");
	}
	else
	{
		TrefC[pos] = TrefC[pos+1];
		TquanC[pos] = TquanC[pos+1];
		return nArticle - 1;
	}
}

int ResetPanier(int TrefC[],int TquanC[],int nArticle)
{
	int i;
	for (i=0;i<=nArticle;i++)
	{
		TrefC[i]=0;
		TquanC[i]=0;
	}
	return 0;
}

int posRefC(int TrefC[], int n, int refR,int nArticle,int Tref[])
{
	int i,pos;
	for (i=0;i <nArticle;i++)
	{
		pos = posRef(Tref,TrefC[i],n);
		if (refR == TrefC[i]) return i;
	}
	return -1;
}