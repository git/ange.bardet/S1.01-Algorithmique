#include <stdio.h>

/**
*\file FonctionsResponsable.h
*\brief fichier.h qui contient les déclarations des fonctions concernant le responsable.
*\author BARDET Ange & Cyriaque SEGERIE
*\date 11 oct 2023
*/

/**
*\brief Demande l'article a ajouter ainsi que son poid, son volume et son prix a l'utilisateur et l'ajoute au fichier "articles.don".
*\param void Rien
*\return Rien
*/
int AjoutArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n);

/**
*\brief Demande un article au responsable puis modifie son poid, son volume et son prix dans les tables.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in,out] Tpoid tableau d'entier, poids des produits.
*\param[in,out] Tvolume tableau d'entier, volumes des produits.
*\param[in,out] Tprix tableau d'entier, prix des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return Rien
*/
void ModifArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n);

/**
*\brief Récupère le contenu du fichier "articles.don" et l'ajoute au tables concernées (Tref,Tpoid,Tvolume,Tprix)
*\param[out] Tref tableau d'entier, réfèrences des produits.
*\param[out] Tpoid tableau d'entier, poids des produits.
*\param[out] Tvolume tableau d'entier, volumes des produits.
*\param[out] Tprix tableau d'entier, prix des produits.
*\return entier, taille logique des tableaux concernant les produits.
*/
int TableArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[]);

/**
*\brief Affiche la référence, le poid, le volume et le prix de chaque produit.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] Tpoid tableau d'entier, poids des produits.
*\param[in] Tvolume tableau d'entier, volumes des produits.
*\param[in] Tprix tableau d'entier, prix des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return Rien
*/
void AffichTable(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n);

/**
*\brief Demande un article au client puis affiche sa référence, son poid, son volume et son prix.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] Tpoid tableau d'entier, poids des produits.
*\param[in] Tvolume tableau d'entier, volumes des produits.
*\param[in] Tprix tableau d'entier, prix des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return Rien
*/
void AffichArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n);

/**
*\brief Demande un article au client puis le supprime des tables.
*\param[in,out] Tref tableau d'entier, réfèrences des produits.
*\param[out] Tpoid tableau d'entier, poids des produits.
*\param[out] Tvolume tableau d'entier, volumes des produits.
*\param[out] Tprix tableau d'entier, prix des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return entier, taille logique des tableaux concernant les produits - 1.
*/
int SupprArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n);

/**
*\brief Recherche la position de refR dans le tableau Tref.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] refR entier, réfèrence recherché dans le tableau référence.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return entier, position de la réfèrence recherché.
*/
int posRef(int Tref[],int refR, int n);

/**
*\brief Ecrit dans le fichier "articles.don" le contenu des tables.
*\param[in] Tref tableau d'entier, réfèrences des produits.
*\param[in] Tpoid tableau d'entier, poids des produits.
*\param[in] Tvolume tableau d'entier, volumes des produits.
*\param[in] Tprix tableau d'entier, prix des produits.
*\param[in] n entier, taille logique des tableaux concernant les produits.
*\return rien.
*/
void EcrireFichier(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n);

/**
*\brief lance le menu global de la partie gestion fichier client.
*\param void rien
*\return rien
*/
void modifclientglobal (void);

/**
*\brief charge les tableaux avec "fichierclient.don".
*\param[in,out] tabNC tableau d'entier, contient les identifiant des cartes clients.
*\param[in,out] tabCAG tableau de flottant, contient les cagnottes des clients.
*\param[in,out] tabsuspension tableau d'entier, contient informations sur la suspension des cartes.
*\param[in] tmax entier, taille physique des tableaux.
*\return la taille logique.
*/
int tableaucharge(int tabNC[],float tabCAG[],int tabsuspension[],int tmax);

/**
*\brief permet d'ajouter un client dans les tableaux.
*\param[in,out] tabNC tableau d'entier, contient les identifiant des cartes clients.
*\param[in,out] tabCAG tableau de flottant, contient les cagnottes des clients.
*\param[in,out] tabsuspension tableau d'entier, contient informations sur la suspension des cartes.
*\param[in] tmax entier, taille physique des tableaux.
*\param[in,out] tlogique pointeur de la taille logique , modifier lors de l'ajout.
*\return code d'erreur si une erreur apparaît.
*/
int ajoutClient (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique,int tmax);

/**
*\brief permet de suspendre OU désuspendre un client.
*\param[in,out] tabNC tableau d'entier, contient les identifiant des cartes clients.
*\param[in,out] tabCAG tableau de flottant, contient les cagnottes des clients.
*\param[in,out] tabsuspension tableau d'entier, contient informations sur la suspension des cartes.
*\param[in] tmax entier, taille physique des tableaux.
*\param[in,out] tlogique pointeur de la taille logique , modifier lors d'un l'ajout.
*\return code d'erreur si une erreur apparaît.
*/
int suspensioncarte (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique,int tmax);

/**
*\brief permet de supprimer un client.
*\param[in,out] tabNC tableau d'entier, contient les identifiant des cartes clients.
*\param[in,out] tabCAG tableau de flottant, contient les cagnottes des clients.
*\param[in,out] tabsuspension tableau d'entier, contient informations sur la suspension des cartes.
*\param[in] tmax entier, taille physique des tableaux.
*\param[in,out] tlogique pointeur de la taille logique.
*\return code d'erreur si une erreur apparaît.
*/
int supressionclient (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique,int tmax);

/**
*\brief permet d'afficher le contenu des tableaux.
*\param[in] tabNC tableau d'entier, contient les identifiant des cartes clients.
*\param[in] tabCAG tableau de flottant, contient les cagnottes des clients.
*\param[in] tabsuspension tableau d'entier, contient informations sur la suspension des cartes.
*\param[in] tlogique pointeur de la taille logique.
*\return code d'erreur si une erreur apparaît.
*/
int affichageall (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique);

/**
*\brief permet d'afficher le contenu d'un tableau spécifique.
*\param[in] tabNC tableau d'entier, contient les identifiant des cartes clients.
*\param[in] tabCAG tableau de flottant, contient les cagnottes des clients.
*\param[in] tabsuspension tableau d'entier, contient informations sur la suspension des cartes.
*\param[in] tlogique pointeur de la taille logique.
*\return code d'erreur si une erreur apparaît.
*/
int affichagesolo (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique);

/**
*\brief vérifie la présence d'un fichier , et si non le crée.
*\param void rien
*\return rien
*/
int creationfichier(void);

/**
*\brief lorsque l'utilisateur quitte le global , écrit les tableaux dans le fichier en écrasant le contenue précédent.
*\param void rien
*\return rien
*/
int enregistrement (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique);