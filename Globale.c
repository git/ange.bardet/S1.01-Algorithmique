#include "FonctionsClient.h"
#include "FonctionsResponsable.h"

/**
*\file Globale.c
*\brief le fichier.c qui gère l'affichage global de l'application.
*\author BARDET Ange
*\date 11 oct 2023
*/


/**
*\brief Permet a l'utilisateur de sélectionner la partie responsable ou client de l'application.
*\param void Rien
*\return Rien
*/
void global(void)
{
	int n=0,rep,codeC;
	int Tref[100]={0};
	float Tpoid[100]={0},Tvolume[100]={0},Tprix[100]={0},volV,chargeV,cagnotteC;
	n = TableArticle(Tref,Tpoid,Tvolume,Tprix);
	printf("Responsable / Client ? (R[0]/C[1])");
	scanf("%d",&rep);
	while (rep < 0 || rep > 1)
		{
			printf("Erreur; (R[0]/C[1]) ");
			scanf("%d",&rep);
		}
	if (rep == 0)
	{
		printf("Bienvenue Mr. le responsable :)\n");
        printf("Que voulez vous faire ? (Gestion d'articles[0]/Gestion de clients[1]/quitter[9]) ");
		scanf("%d",&rep);
        while (rep != 9)
		{
			while ((rep < 0 || rep > 1) && rep != 9)
			{
				printf("Erreur; (Gestion d'articles[0]/Gestion de clients[1]/quitter[9]) ");
				scanf("%d",&rep);
			}
			if (rep == 0)
			{
				printf("Que voulez vous faire ? (afficher[0]/afficher article[1]/ajouter[2]/modifier[3]/supprimer[4]/quitter[9]) ");
                scanf("%d",&rep);
                while (rep != 9)
                {
                    while ((rep < 0 || rep > 4) && rep != 9)
                    {
                        printf("Erreur; (afficher[0]/afficher article[1]/ajouter[2]/modifier[3]/supprimer[4]/quitter[9]) ");
                        scanf("%d",&rep);
                    }
                    if (rep == 0)
                    {
                        AffichTable(Tref,Tpoid,Tvolume,Tprix,n);
                        printf("\n");
                    }
                    if (rep == 1)
                    {
                        AffichArticle(Tref,Tpoid,Tvolume,Tprix,n);
                        printf("\n");
                    }
                    if (rep == 2)
                    {
                        n = AjoutArticle(Tref,Tpoid,Tvolume,Tprix,n);
                        printf("\n");
                    }
                    if (rep == 3)
                    {
                        ModifArticle(Tref,Tpoid,Tvolume,Tprix,n);
                        printf("\n");
                    }
                    if (rep == 4)
                    {
                        n = SupprArticle(Tref,Tpoid,Tvolume,Tprix,n);
                        printf("\n");
                    }
                    if (rep != 9)
                    {
                        printf("Que faire ? (afficher[0]/afficher article[1]/ajouter[2]/modifier[3]/supprimer[4]/quitter[9]) ");
                        scanf("%d",&rep);
                    }
                }
                rep=0;
			}
			if (rep == 1)
			{
				modifclientglobal();
			}
			if (rep != 9)
			{
				printf("Que faire ? (Gestion d'articles[0]/Gestion de clients[1]/quitter[9]) ");
				scanf("%d",&rep);
			}
		}
	}
	if (rep == 1)
	{
		printf("Bienvenue Mr. le client :)\n");
		printf("Voici la liste des articles disponibles : \n");
		AffichTable(Tref,Tpoid,Tvolume,Tprix,n);
		printf("\n");
		volV = initClient(&chargeV,&codeC,&cagnotteC);
		printf("Que voulez vous faire ? (Ajout[0]/ModifierArticle[1]/SupprimerArticle[2]/AfficherPanier[3]/ResetPanier[4]/quitter[9]) ");
		scanf("%d",&rep);
		int TrefC[200]={0},TquanC[200]={0},nArticle=0;
		while (rep != 9)
		{
			while ((rep < 0 || rep > 4) && rep != 9)
			{
				printf("Erreur; (Ajout[0]/ModifierArticle[1]/SupprimerArticle[2]/AfficherPanier[3]/ResetPanier[4]/quitter[9]) ");
				scanf("%d",&rep);
			}
			if (rep == 0)
			{
				nArticle = ajoutPanier(TrefC,TquanC,nArticle,Tref,n);
				affichPanier(TrefC,TquanC,nArticle,Tref,Tpoid,Tvolume,Tprix,chargeV,volV,n,cagnotteC);
				printf("\n");
			}
			if (rep == 1)
			{
				ModifArticlePanier(TrefC,TquanC,nArticle,Tref,n);
				affichPanier(TrefC,TquanC,nArticle,Tref,Tpoid,Tvolume,Tprix,chargeV,volV,n,cagnotteC);
				printf("\n");
			}
			if (rep == 2)
			{
				nArticle = SupprArticlePanier(TrefC,TquanC,nArticle,Tref,n);
				affichPanier(TrefC,TquanC,nArticle,Tref,Tpoid,Tvolume,Tprix,chargeV,volV,n,cagnotteC);
				printf("\n");
			}
			if (rep == 3)
			{
				affichPanier(TrefC,TquanC,nArticle,Tref,Tpoid,Tvolume,Tprix,chargeV,volV,n,cagnotteC);
				printf("\n");
			}
			if (rep == 4)
			{
				nArticle = ResetPanier(TrefC,TquanC,nArticle);
				printf("\n");
			}
			if (rep != 9)
			{
				printf("Que faire ? (Ajout[0]/ModifierArticle[1]/SupprimerArticle[2]/AfficherPanier[3]/ResetPanier[4]/quitter[9]) ");
				scanf("%d",&rep);
			}

		}
	}
	EcrireFichier(Tref,Tpoid,Tvolume,Tprix,n);
	printf("Au revoir !\n");
}
int main(void)
{
	global();
	return 0;
}