#include "FonctionsResponsable.h"
#include <stdlib.h>
#include <stdio.h>

/**
*\file FonctionsResponsable.c
*\brief fichier.c qui contient toutes les fonctions concernant le responsable.
*\author BARDET Ange & Cyriaque SEGERIE
*\date 11 oct 2023
*/

int AjoutArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n)
{
	int ref,pos;
	float poid,volume,prix;
	printf("Saisir la référence de l'article (-1 pour arrêter): ");
	scanf("%d",&ref);
	/*demander numéro client et regarder cagnotte*/
	while (ref < -1)
	{
		printf("Erreur ; Le numéro doit être positif (ou -1 pour arrêter); retapez : ");
		scanf("%d",&ref);
	}
	pos = posRef(Tref,ref,n);
	if (pos >= 0)
	{
		printf("L'article fait déja partie de la base de donnée.");
		return n;
	}
	while (ref >= 0)
	{
		printf("Saisir le poid(kg): ");
		scanf("%f",&poid);
		while (poid < 0)
		{
			printf("Erreur ; Le poid doit être positif ; retapez : ");
			scanf("%f",&poid);
		}

		printf("Saisir le volume: ");
		scanf("%f",&volume);
		while (volume < 0)
		{
			printf("Erreur ; Le volume doit être positif ; retapez : ");
			scanf("%f",&volume);
		}

		printf("Saisir le prix: ");
		scanf("%f",&prix);
		while (prix < 0)
		{
			printf("Erreur ; Le prix doit être positif ; retapez : ");
			scanf("%f",&prix);
		}	
		
		Tref[n] = ref;
		Tpoid[n] = poid;
		Tvolume[n] = volume;
		Tprix[n] = prix;
		printf("Saisir la référence de l'article (-1 pour arrêter): ");
		scanf("%d",&ref);
		while (ref < -1)
		{
			printf("Erreur ; Le numéro doit être positif (ou -1 pour arrêter); retapez : ");
			scanf("%d",&ref);
		}
	}
	printf("Fin de l'ajout.\n");
	return n+1;
}

int TableArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[])
{
	int ref,i=0;
	float poid,volume,prix;

	FILE *flot;
	flot = fopen("articles.don","r");
	if (flot == NULL)
		{
			printf("Problème avec la lecture du fichier\n");
			exit(1);
		}

	fscanf(flot,"%d %f %f %f",&ref,&poid,&volume,&prix);
	while(feof(flot) == 0)
	{
		Tref[i] = ref;
		Tpoid[i] = poid;
		Tvolume[i] = volume;
		Tprix[i] = prix;
		i++;
		fscanf(flot,"%d %f %f %f",&ref,&poid,&volume,&prix);
	}
	fclose(flot);
	return i;
}

void AffichTable(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n)
{
	int i;
	printf("référence\tpoids(kg)\tvolume(l)\tprix Unitaire\n");
	for (i=0;i < n;i++)
	{
		printf("%d\t\t%.2f\t\t%.3f\t\t%.2f\n",Tref[i],Tpoid[i],Tvolume[i],Tprix[i]);
	}
}

void AffichArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n)
{
	int refR,pos;
	printf("Afficher quel article ? ");
	scanf("%d",&refR);
	pos = posRef(Tref,refR,n);
	if (pos == -1)
	{
		printf("L'article n'existe pas dans la base de donnée.");
	}
	else
	{
		printf("%d\t\t%.2f\t\t%.3f\t\t%.2f\n",Tref[pos],Tpoid[pos],Tvolume[pos],Tprix[pos]);
	}
}

void ModifArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n)
{
	int refR;
	printf("Saisir la référence de l'article a modifier: ");
	scanf("%d",&refR);
	while (refR < -1)
	{
		printf("Erreur ; Le numéro doit être positif; retapez : ");
		scanf("%d",&refR);
	}
	int i;
	for (i=0;i < n;i++)
	{
	if (Tref[i] == refR)
		{
		printf("Saisir le nouveau poid(kg): ");
		scanf("%f",&Tpoid[i]);
		while (Tpoid[i] < 0)
		{
			printf("Erreur ; Le poid doit être positif ; retapez : ");
			scanf("%f",&Tpoid[i]);
		}

		printf("Saisir le nouveau volume: ");
		scanf("%f",&Tvolume[i]);
		while (Tvolume[i] < 0)
		{
			printf("Erreur ; Le volume doit être positif ; retapez : ");
			scanf("%f",&Tvolume[i]);
		}

		printf("Saisir le nouveau prix: ");
		scanf("%f",&Tprix[i]);
		while (Tprix[i] < 0)
		{
			printf("Erreur ; Le prix doit être positif ; retapez : ");
			scanf("%f",&Tprix[i]);
		}
		}
	}
}

int SupprArticle(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n)
{
	int refR,pos;
	printf("Saisir la référence de l'article a supprimer: ");
	scanf("%d",&refR);
	while (refR < -1)
	{
		printf("Erreur ; Le numéro doit être positif; retapez : ");
		scanf("%d",&refR);
	}
	pos = posRef(Tref,refR,n);
	if (pos == -1)
	{
		printf("L'article n'existe pas dans la base de donnée.");
		return n;
	}
	else
	{
		int i;
		for (i=pos;i<=n;i++)
		{			
			Tref[i] = Tref[i+1];
			Tpoid[i] = Tpoid[i+1];
			Tvolume[i] = Tvolume[i+1];
			Tprix[i] = Tprix[i+1];
		}
		return n-1;
	}
}

int posRef(int Tref[],int refR, int n)
{
	int i;
	for (i=0;i < n;i++)
	{
		if (Tref[i] == refR)
			return i;
	}
	return -1;
}

void EcrireFichier(int Tref[], float Tpoid[],float Tvolume[],float Tprix[],int n)
{
	FILE *flot;
	flot = fopen("articles.don","w");
	if (flot == NULL)
		{
			printf("Problème avec la création du fichier\n");
			exit(1);
		}
	int i;
	for (i=0;i < n;i++) fprintf(flot,"%d\t%.2f\t%.3f\t%.2f\n",Tref[i],Tpoid[i],Tvolume[i],Tprix[i]);
	fclose(flot);
	printf("Fichier écrit.\n");
}

void modifclientglobal (void)
{
	int n,choix,tmax=100,coderreur;
	int tabNC [100]={0}, tabsuspension [100]={0};
	float tabCAG [100]={0};

	coderreur=creationfichier();

	n=tableaucharge(tabNC,tabCAG,tabsuspension,tmax);
	if (n==-1 || n==-2)
	{
		printf("tableau non chargée, erreur\n");
		return;
	}
	printf("Bienvenue dans l'application de modulation client .\nque souhaitez vous faire aujourd'hui ?\n");

	while(choix!=9)
	{	
		printf("\n");
		printf("Appuyez sur '1' pour ajouter un client,\nsur '2' pour changer l'état de suspension d'une carte client,\nsur '3' pour supprimer un client ,\nsur '4' pour afficher le fichier client,\nsur '5' pour chercher un seul client dans le fichier,\nOu sur '9' pour enregistrer et sortir du programme !\n");
		scanf("%d",&choix);
		while (choix!=1 && choix!=2 && choix!=3 && choix!=4  && choix!=5 && choix!=9)
		{
			printf("Entrée incorrect, veuillez ressasisir.\n");
			scanf("%d",&choix);
		}
		if (choix==1)
		{
			coderreur=ajoutClient(tabNC,tabCAG,tabsuspension,&n,tmax);
		}
		
		if (choix==2)
		{
			coderreur=suspensioncarte(tabNC,tabCAG,tabsuspension,&n,tmax);
		}
		
		if (choix==3)
		{
			coderreur=supressionclient(tabNC,tabCAG,tabsuspension,&n,tmax);
		}

		if (choix==4)
		{
			coderreur=affichageall(tabNC,tabCAG,tabsuspension,&n);
		}
		if (choix==5)
		{
			coderreur=affichagesolo(tabNC,tabCAG,tabsuspension,&n);
		}
	}
    coderreur=enregistrement(tabNC,tabCAG,tabsuspension,&n);
	return;

}

int tableaucharge(int tabNC[],float tabCAG[],int tabsuspension[],int tmax)
{
	int compteur=0,nc,suspension;
	float cagnotte;

	FILE *fichierclient;

	fichierclient=fopen("fichierclient.don","r");
	if (fichierclient==NULL)
	{
		printf("\n");
		printf("Probléme d'ouverture");
		return -2;
	}

	fscanf(fichierclient,"%d%f%d",&nc,&cagnotte,&suspension);
	while (feof(fichierclient)==0)
	{
		if (compteur>tmax)
	    {
	    	printf("erreur de taille\n");
	        return -1;
	    }
		tabNC[compteur]=nc;
	    tabCAG[compteur]=cagnotte;
	    tabsuspension[compteur]=suspension;
	    compteur=compteur+1;
	    fscanf(fichierclient,"%d%f%d",&nc,&cagnotte,&suspension);
	}
	fclose (fichierclient);
	return compteur-1;
}

int ajoutClient (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique,int tmax)

{
	int numeroclient,suspension,compteur=0;
	float cagnotte;

	if (*tlogique+1>tmax)
	{
		printf("tableau trop petit\n");
		return -1;
	}

	printf("ajout d'un client au programme de fidélité...\nquel est le numéro de ce client ?\n'999' pour quitter.\n");
	scanf("%d",&numeroclient);
	if (numeroclient==999)
	{
		printf("retour au menu global\n");
		return 0;
	}
	while (numeroclient<0)
	{
		printf("le numéro du client ne peut être négatif , réessayez\n'999' pour quitter.\n");
	    scanf("%d",&numeroclient);
		if (numeroclient==999)
		{
			printf("retour au menu global\n");
			return 0;
		}
	}

	while (compteur<=*tlogique)
	{
		if (tabNC[compteur]==numeroclient)
		{
			while(tabNC[compteur]==numeroclient)
			{
				printf("le numéro client existe déja ,veuillez ressaissir\n");
			    scanf("%d",&numeroclient);
			}
		}
		compteur=compteur+1;
		
	}

	*tlogique=*tlogique+1;
	tabNC[*tlogique]=numeroclient;
	tabCAG[*tlogique]=0.00;
	tabsuspension[*tlogique]=0;

	

	printf("client ajouté au programme de fidélité !\n");
	return 0;

}
	
int suspensioncarte (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique,int tmax)

{

	int nouvellecarte,suspension=2,compteur=0,compteurCAG,recherche;

    printf("quel carte souhaitez vous suspendre ou désuspendre (entrez numéro client)\n'999' pour quitter.\n");
    scanf("%d",&recherche);
	if (recherche==999)
		{
			printf("retour au menu global\n");
			return 0;
		}
    while (recherche<0)
    {
    	printf("numéro non valide , veuillez réessayez\n");
    	scanf("%d",&recherche);
		if (recherche==999)
		{
			printf("retour au menu global\n");
			return 0;
		}
    }

    for (compteur=0;compteur<=*tlogique;compteur++)
    {
    	if (tabNC[compteur]==recherche)
    	{
    		compteurCAG=compteur;
    		printf("l'état de la carte de ce client est %d\n",tabsuspension[compteur]);
    	    printf("modifiez l'état (0 pour non suspendu) (1 pour suspendu)\n");
    	    scanf("%d",&suspension);
    	    while (suspension!=1 && suspension !=0)
    	    {  
    	    	printf("état de suspension non valide , retapez.\n");
    			scanf("%d",&suspension);
    	    }
    	    if (tabsuspension[compteur]==suspension)
    	    {
    	    	printf("état de suspension est déja de %d\n",suspension);
    	    	return -4;
    	    }
    	    tabsuspension[compteur]=suspension;
    	}
    }

    if (suspension!=0 && suspension!=1)
    {
    	printf("le numéro client n'existe pas\n");
    	return -7;
    }

    if (tabsuspension[compteurCAG]==1)
    {
		if (*tlogique+1>tmax)
		{
			printf("tableau trop petit\n");
			return -1;
		}

    	*tlogique=*tlogique+1;
	    
		printf("création d'une nouvelle carte pour le client %d\n",tabNC[compteurCAG]);
		printf("numéro de la nouvelle carte ?\n");
		scanf("%d",&nouvellecarte);
		while (nouvellecarte<0)
		{
			printf("ne peut étre négatif\n");
			scanf("%d",&nouvellecarte);
		}
		for (compteur=0;compteur<=*tlogique;compteur++)

			while (tabNC[compteur]==nouvellecarte)
			{
				printf("le client existe déja, ressaisissez\n");
				scanf("%d",&nouvellecarte);
				while(nouvellecarte<0)
				{
					printf("le numéro ne peut étre négatif , rééssayez.\n");
					scanf("%d",&nouvellecarte);
				}
			}

		tabNC[*tlogique]=nouvellecarte;
		tabCAG[*tlogique]=tabCAG[compteurCAG];
		tabsuspension[*tlogique]=0;

		printf("état de suspension modifié et nouvelle carte crée !\n");
		return 0;
	}
	printf("état de suspension modifié !\n");
   	return 0;
}

int supressionclient (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique,int tmax)
{

	int compteur,recherche,nt=1;
	printf("\n");
	printf("quel est le numéro du client que vous souhaitez supprimer ?\n'999' pour quitter.\n");
	scanf("%d",&recherche);
	if (recherche==999)
	{
		printf("retour au menu global\n");
		return 0;
	}
	while(recherche<0)
	{
		printf("ne peut étre négatif\n'999' pour quitter.\n");
		scanf("%d",&recherche);
		if (recherche==999)
		{
			printf("retour au menu global\n");
			return 0;

		}
	}

	for (compteur=0;compteur<=*tlogique;compteur++)
		if (tabNC[compteur]==recherche)
		{
			while (compteur<=*tlogique)
			{
				tabNC[compteur]=tabNC[compteur+1];
				tabCAG[compteur]=tabCAG[compteur+1];
				tabsuspension[compteur]=tabsuspension[compteur+1];
				compteur=compteur+1;
				nt=0;

			}
			*tlogique=*tlogique-1;
		}

	if (nt!=0)
	{
		printf("le numéro client n'existe pas\n");
		return -4;
	}
	printf("client supprimé.\n");
	return 0;

}

int affichageall (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique)
{
	int compteur;
	printf("\n");
	for (compteur=0;compteur<=*tlogique;compteur++)
		printf("%d\t%.2f\t%d\n",tabNC[compteur],tabCAG[compteur],tabsuspension[compteur]);
	return 0;
}

int affichagesolo (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique)
{
	int rechercher,compteur=0;
	float cagnotte;

	printf("\n");
	printf("quel client recherchez vous ?\n'999' pour quitter.\n");
	scanf("%d",&rechercher);
	if (rechercher==999)
	{
		printf("retour au menu global\n");
		return 0;
	}
	while(rechercher<0)
	{
		printf("ne peut étre négatif\n'999' pour quitter.\n");
		scanf("%d",&rechercher);
		if (rechercher==999)
		{
			printf("retour au menu global\n");
			return 0;
		}
		
	}
	printf("\n");

	while (compteur<*tlogique)
	{	
		if (tabNC[compteur]==rechercher)
		{
			printf("%d\t%.2f\t%d\n",tabNC[compteur],tabCAG[compteur],tabsuspension[compteur]);
			return 0;
		}
		compteur=compteur+1;
	}
	printf("ce client n'existe pas\n");
	return 0;
}

int creationfichier(void)
{
	FILE *flot;
	flot=fopen("fichierclient.don","a");
	fclose(flot);
	return 0;

}

int enregistrement (int tabNC[],float tabCAG[],int tabsuspension[],int *tlogique)
{
    int compteur;

    FILE *flot;
    flot=fopen("fichierclient.don","w");
    if (flot==NULL)
    {
        printf("probléme d'ouverture fichier.");
        return -1;
    }
    for (compteur=0;compteur<=*tlogique;compteur++)
        fprintf(flot,"%d\t%.2f\t%d\n",tabNC[compteur],tabCAG[compteur],tabsuspension[compteur]);
    return 0;
}